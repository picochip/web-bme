load('api_mqtt.js');
load('api_timer.js');
load('api_sys.js');
load('api_config.js');
load('api_rpc.js');
load('api_gpio.js');
load('api_bme280.js');
RPC.addHandler('myRpc', function (jsonText) {
    let textSaved = JSON.stringify(jsonText);
    if (jsonText.id === 'input1') {
        Cfg.set({ input1: textSaved });
    } else if (jsonText.id === 'input2') {
        Cfg.set({ input2: textSaved });
    } else if (jsonText.id === 'input3') {
        Cfg.set({ input3: textSaved });
    } else if (jsonText.id === 'input4') {
        Cfg.set({ input4: textSaved });
    } else if (jsonText.id === 'setNet') {
        let setNet = jsonText;
        Cfg.set({ wifi: { ap: { ssid: setNet.nameAp } } });
        Cfg.set({ wifi: { ap: { pass: setNet.passwordAp } } });
        Cfg.set({ wifi: { sta: { ssid: setNet.nameSta } } });
        Cfg.set({ wifi: { sta: { pass: setNet.passwordSta } } });
        Cfg.set({ mqtt: { server: setNet.ipMqtt } });
    };
    return jsonText;
});

RPC.addHandler('myRpcGetValue', function (idSetValue) {
    let jsonText = {};
    if (idSetValue === 'generalSettings') {
        jsonText = {
            nameAp: Cfg.get('wifi.ap.ssid'),
            passwordAp: Cfg.get('wifi.ap.pass'),
            nameSta: Cfg.get('wifi.sta.ssid'),
            passwordSta: Cfg.get('wifi.sta.pass'),
            ipMqtt: Cfg.get('mqtt.server'),
        }
    } else if (idSetValue === 'input1') {
        jsonText = {
            setTime: Cfg.get('input1'),
        };
    } else if (idSetValue === 'input2') {
        jsonText = {
            setTime: Cfg.get('input2'),
        };
    } else if (idSetValue === 'input3') {
        jsonText = {
            setTime: Cfg.get('input3'),
        };
    } else if (idSetValue === 'input4') {
        jsonText = {
            setTime: Cfg.get('input4'),
        };
    }
    return jsonText;
});






let regulatorChannel = {
    input1: { // Light
        nameChannel: 'input1',
        pin: 12,
        stateRelay: 0,
    },
    input2: { //watering
        nameChannel: 'input2',
        pin: 14,
        stateRelay: 0,
    },
    input3: { //fun
        nameChannel: 'input3',
        pin: 13,
        stateRelay: 0,
    },
    input4: { //heating
        nameChannel: 'input4',
        pin: 15,
        stateRelay: 0,
    },
};

GPIO.setup_output(regulatorChannel.input1.pin, 1);
GPIO.setup_output(regulatorChannel.input2.pin, 1);
GPIO.setup_output(regulatorChannel.input3.pin, 1);
GPIO.setup_output(regulatorChannel.input4.pin, 1);

let bme = BME280.createI2C(0x76);
let bmeValue = null;
let bmePressure = null;
let bmeTemperature = null;
let bmeHumidity = null;

function switchOutput1() {
    let timeSetParse = JSON.parse(Cfg.get('input1'));
    let timeFromSet = JSON.parse(timeSetParse.from.hours) * 60 * 60 + JSON.parse(timeSetParse.from.minut) * 60 + JSON.parse(timeSetParse.from.second);
    let timeToSet = JSON.parse(timeSetParse.to.hours) * 60 * 60 + JSON.parse(timeSetParse.to.minut) * 60 + JSON.parse(timeSetParse.to.second);
    let timeNow = getTimeDay();



    function regulateInput1() {
        if (timeToSet === timeFromSet) {
            toEquallyFrom1();
        } else if (timeToSet > timeFromSet) {
            toMoreFrom1();
        } else if (timeToSet < timeFromSet) {
            toLessFrom1();
        }
    }


    function toEquallyFrom1() {
        GPIO.write(regulatorChannel.input1.pin, 0);
        print('input1 on t=f');
        regulatorChannel.input1.stateRelay = true;
        MQTT.pub('/input1Switch', "on", 0);
    }

    function toMoreFrom1() {
        if (timeNow > timeFromSet && timeNow < timeToSet) {
            updateState1(true);
        } else if (timeNow < timeFromSet || timeNow > timeToSet) {
            updateState1(false);
        }
    }

    function toLessFrom1() {
        print('timeNow = ', timeNow, 'timeToSet = ', timeToSet, 'timeFromSet =', timeFromSet)
        if (timeNow > timeToSet && timeNow < timeFromSet) {
            updateState1(false);
        } else if (timeNow < timeToSet || timeNow > timeFromSet) {
            updateState1(true);
        }
    }


    function updateState1(newState) {
        print('state relay = ', regulatorChannel.input1.stateRelay, '    new state = ', newState);
        if (regulatorChannel.input1.stateRelay !== newState) {
            if (newState === true) {
                GPIO.write(regulatorChannel.input1.pin, 0);
                MQTT.pub('/input1Switch', "on", 0);
                print('input1 on');

            } else {
                GPIO.write(regulatorChannel.input1.pin, 1);
                MQTT.pub('/input1Switch', "off", 0);
                print('input1 off');

            }
            let topic = '/' + regulatorChannel.input1.nameChannel;
            MQTT.pub(topic, JSON.stringify(newState), 0);
            regulatorChannel.input1.stateRelay = newState;
        }
    }

    if (timeSetParse.state === '1') {
        regulateInput1();
    } else {
        GPIO.write(regulatorChannel.input1.pin, 1);
        MQTT.pub('/input1Switch', "off", 0);
        print('input1 channel off');
    }

};





function switchOutput2() {

    let settingWateringParse = JSON.parse(Cfg.get('input2'));
    let timeNow = getTimeDay();
    let segment = timeNow % (86400 / JSON.parse(settingWateringParse.quantity));

    function regulateInput2() {

        if (settingWateringParse.quantity === '25') {
            GPIO.write(regulatorChannel.input2.pin, 0);
            print('on channel 2');
            regulatorChannel.input2.stateRelay = true;
        } else if (segment <= settingWateringParse.duration) {
            updateState(true);
        } else if (segment > settingWateringParse.duration) {
            updateState(false);
        };
    }
    function updateState(newState) {
        if (regulatorChannel.input2.stateRelay !== newState) {
            if (newState === true) {
                GPIO.write(regulatorChannel.input2.pin, 0);
                print('on channel 2');
            } else {
                GPIO.write(regulatorChannel.input2.pin, 1);
                print('off channel 2');
            }
            let topic = '/' + regulatorChannel.input2.nameChannel;
            MQTT.pub(topic, JSON.stringify(newState), 0);
            regulatorChannel.input2.stateRelay = newState;
        }
    }

    if (settingWateringParse.state === '1') {
        regulateInput2();
    } else {
        GPIO.write(regulatorChannel.input2.pin, 1);
        print('off all channel 2');
    }
}


function switchOutput3() {
    let timeSetParse = JSON.parse(Cfg.get('input3'));
    let timeFromSet = JSON.parse(timeSetParse.from.hours) * 60 * 60 + JSON.parse(timeSetParse.from.minut) * 60 + JSON.parse(timeSetParse.from.second);
    let timeToSet = JSON.parse(timeSetParse.to.hours) * 60 * 60 + JSON.parse(timeSetParse.to.minut) * 60 + JSON.parse(timeSetParse.to.second);
    let timeNow = getTimeDay();

    function regulateInput3() {
        if (timeToSet === timeFromSet) {
            GPIO.write(regulatorChannel.input3.pin, 0);
            print('To=From, on channel 3');
            regulatorChannel.input3.stateRelay = true;
        } else if (timeToSet > timeFromSet) {
            if (timeNow > timeFromSet && timeNow < timeToSet) {
                updateState(true);
            } else if (timeNow < timeFromSet || timeNow > timeToSet) {
                updateState(false);
            }
        } else if (timeToSet < timeFromSet) {
            if (timeNow > timeToSet && timeNow < timeFromSet) {
                updateState(false);
            } else if (timeNow < timeToSet || timeNow > timeFromSet) {
                updateState(true);
            }
        }
    }

    function updateState(newState) {
        if (regulatorChannel.input3.stateRelay !== newState) {
            print('state relay = ', regulatorChannel.input3.stateRelay, '    new state = ', newState);
            if (newState === true) {
                GPIO.write(regulatorChannel.input3.pin, 0);
                print('relay on channel 3');
            } else {
                GPIO.write(regulatorChannel.input3.pin, 1);
                print('relay off channel 3');
            }
            let topic = '/' + regulatorChannel.input3.nameChannel;
            MQTT.pub(topic, JSON.stringify(newState), 0);
            regulatorChannel.input3.stateRelay = newState;
        }
    }

    if (timeSetParse.state === '1') {
        regulateInput3();
    } else {
        GPIO.write(regulatorChannel.input3.pin, 1);
        print('channel 3 off');
    }
};





function switchOutput4() {
    let settingHeatingParse = JSON.parse(Cfg.get('input4'));
    let set = JSON.parse(settingHeatingParse.set);
    function regulateInput4() {
        if (bmeTemperature <= set) {
            updateState(true);
        } else if (bmeTemperature > (set - 0.5)) {
            updateState(false);
        };
    }

    function updateState(newState) {
        if (regulatorChannel.input4.stateRelay !== newState) {
            if (newState === true) {
                GPIO.write(regulatorChannel.input4.pin, 0);
                print('on channel 4');
            } else {
                GPIO.write(regulatorChannel.input4.pin, 1);
                print('off channel 4');
            }
            let topic = '/' + regulatorChannel.input4.nameChannel;
            MQTT.pub(topic, JSON.stringify(newState), 0);
            regulatorChannel.input4.stateRelay = newState;
        }
    }

    if (settingHeatingParse.state === '1') {
        regulateInput4();
    } else {
        GPIO.write(regulatorChannel.input4.pin, 1);
        print('off channel 4');
    }
}

function getTimeDay() {
    let timeNow = (Timer.now() + 7 * 60 * 60) % 86400;
    return timeNow;
}

function getTime() {
    let timeNow = Timer.now() + 7 * 60 * 60;
    return timeNow;
}

let tempFake = 0;

Timer.set(1 * 1000, true, function () {
    tempFake = tempFake + 1;
    let timeNowEsp = JSON.stringify(getTime());
    bmePressure = bme.readPress()
    bmeTemperature = bme.readTemp();
   // bmeTemperature = 22 + Math.sin(tempFake);
    bmeHumidity = bme.readHumid();
    MQTT.pub('/temperature', JSON.stringify(bmeTemperature), 0);
    MQTT.pub('/humidity', JSON.stringify(bmeHumidity), 0);
    MQTT.pub('/Preassure', JSON.stringify(bmePressure), 0);
    MQTT.pub('/dateEsp', timeNowEsp, 0);
    switchOutput1();
    switchOutput2();
    switchOutput3();
    switchOutput4();
}, null);






