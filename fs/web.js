
async function onInputOnChange() {
    const $inputOnValue = document.getElementById('inputOnValue');
    if ($inputOn.value == 1) {
        $inputOnValue.innerHTML = "on";
    } else {
        $inputOnValue.innerHTML = "off";
    }
};


async function onInputClick(settings) {
    const jsonText = JSON.stringify(settings);
    console.log(jsonText);
    let test = await callMyRpc(jsonText);
};

async function callMyRpc(json) {
    const fetchResult = await fetch('/rpc/myRpc', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: json,
    });
    const result = await fetchResult.json();
    return result;
};

async function callMyRpcGetValue(json) {
    const fetchResult = await fetch('/rpc/myRpcGetValue', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: json,
    });
    const result = await fetchResult.json();
    return result;
}

async function getValueInput(idSetting) {
    const jsonText = JSON.stringify(idSetting);
    let valueInput = await callMyRpcGetValue(jsonText);
    return valueInput;
}